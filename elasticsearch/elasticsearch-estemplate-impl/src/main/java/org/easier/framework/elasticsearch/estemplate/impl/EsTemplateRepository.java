package org.easier.framework.elasticsearch.estemplate.impl;

import org.easier.framework.elasticsearch.estemplate.impl.example.EsUser;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.repository.support.ElasticsearchEntityInformation;
import org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository;
import org.springframework.stereotype.Repository;


/**
 * https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/#preface.versions
 * 官网
 */
@Repository
public class EsTemplateRepository extends SimpleElasticsearchRepository<EsUser, Long> {

    public EsTemplateRepository(ElasticsearchEntityInformation<EsUser, Long> metadata, ElasticsearchOperations elasticsearchOperations) {
        super(metadata, elasticsearchOperations);
    }


}
