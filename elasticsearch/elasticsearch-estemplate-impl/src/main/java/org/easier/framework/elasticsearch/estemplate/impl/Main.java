package org.easier.framework.elasticsearch.estemplate.impl;

import org.easier.framework.elasticsearch.estemplate.impl.example.EsUser;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class Main {

    EsTemplateRepository esTemplateRepository;

    public void test() {

        PageRequest pageRequest = PageRequest.of(1, 20, Sort.by(Sort.Order.asc("ss"), Sort.Order.desc("xxx")));
        Page<EsUser> all = esTemplateRepository.findAll(pageRequest);


        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("key", "value");
        boolQueryBuilder.must(matchQueryBuilder);
        esTemplateRepository.search(boolQueryBuilder, pageRequest);
    }
}
