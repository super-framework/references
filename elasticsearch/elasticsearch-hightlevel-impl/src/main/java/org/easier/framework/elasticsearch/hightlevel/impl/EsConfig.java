package org.easier.framework.elasticsearch.hightlevel.impl;

import lombok.Data;
import org.apache.http.HttpHost;


@Data
public class EsConfig {

    /**
     * es 地址
     */
    private HttpHost[] httpHosts;
    /**
     * es 账号
     */
    private String userName;
    /**
     * es 密码
     */
    private String password;

    /**
     * 客户端和服务器建立连接的timeout.
     * 默认值-1
     */
    private Integer connectTimeOut;
    /**
     * 客户端从服务器读取数据的timeout超出预期设定时间
     * 默认值-1
     */
    private Integer socketTimeOut;

    /**
     * 从连接池获取连接的timeout超出预设时间
     * 默认值-1
     */
    private Integer connectionRequestTimeOut;


}
