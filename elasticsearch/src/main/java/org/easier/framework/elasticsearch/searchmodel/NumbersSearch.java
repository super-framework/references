package org.easier.framework.elasticsearch.searchmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumbersSearch extends BaseSearch implements Search {
    private List<Long> value;
    private float boost;
}
