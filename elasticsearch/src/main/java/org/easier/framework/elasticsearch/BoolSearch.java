package org.easier.framework.elasticsearch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.easier.framework.elasticsearch.searchmodel.Search;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BoolSearch implements Search {

    List<Search> searches;
    private OperatorTypes operatorType;
    private int boost;

}
