package org.easier.framework.elasticsearch.page;

import lombok.Data;

/**
 * 排序
 */
@Data
public class SortFun {
    private String key;
    private float factor;
}
