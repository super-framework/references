package org.easier.framework.elasticsearch.searchmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyWordsSearch extends BaseSearch implements Search {

    private List<String> value;
}
