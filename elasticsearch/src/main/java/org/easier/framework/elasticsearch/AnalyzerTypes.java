package org.easier.framework.elasticsearch;

public enum AnalyzerTypes {
    /**
     * ik中文分词
     */
    IK("ik_max_word"),


    /**
     * The standard analyzer divides text into terms on word boundaries,
     * as defined by the Unicode Text Segmentation algorithm. It removes most punctuation,
     * lowercases terms, and supports removing stop words.
     * 标准分析器将文本划分为字边界上的术语，如Unicode文本分割算法所定义。 它删除了大多数标点符号，小写术语，并支持删除停用词。
     * 即单字分词
     */
    STANDARD("standard"),

    /**
     * The simple analyzer divides text into terms whenever it encounters a character which is not a letter.
     * It lowercases all terms.
     * 只要遇到不是字母的字符，简单的分析器就会将文本划分为术语。他转换了所有大写为小写
     *   一般使用欧洲语言,不推荐亚洲语言
     *
     */
    SIMPLE("simple"),

    /**
     * The whitespace analyzer divides text into terms whenever it encounters any whitespace character. It does not lowercase terms.
     * 只要遇到任何空白字符，空白分析器就会将文本划分为术语。 它没有小写术语。
     * 不支持中文；
     */
    WHITESPACE("whitespace"),

    /**
     * 不分词，整个文本为条件
     * The keyword analyzer is a “noop” analyzer that accepts whatever text it is given and outputs the exact same text as a single term.
     * 关键字分析器是一个“noop”分析器，它接受给定的任何文本，并输出与单个术语完全相同的文本。
     *
     */
    KEYWORD("keyword"),


    ENGLISH("english"),

    /**
     * 停用词分析器
     */
    STOP("stop")


    ;


    String name;

    AnalyzerTypes(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
