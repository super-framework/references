package org.easier.framework.elasticsearch.searchmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumberSearch extends BaseSearch implements Search {
    private long value;
    private float boost;

}
