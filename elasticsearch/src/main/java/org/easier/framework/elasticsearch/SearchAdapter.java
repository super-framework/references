package org.easier.framework.elasticsearch;



import org.easier.framework.elasticsearch.page.OrderByInput;
import org.easier.framework.elasticsearch.page.PageContent;
import org.easier.framework.elasticsearch.page.SortFun;

import java.util.List;
import java.util.Map;

public interface SearchAdapter {
    void close();

    /**
     * 创建索引
     *
     * @param name   索引名
     * @param fields 字段列表
     * @return
     */
    boolean createIndex(String name, List<CreateIndexField> fields);


    /**
     * 存在索引
     *
     * @param name 索引名
     * @return
     */
    boolean existsIndex(String name);

    /**
     * 删除索引
     *
     * @param name 索引名
     * @return
     */
    boolean deleteIndex(String name);

    /**
     * 返回索引
     *
     * @param name 索引名
     * @return
     */
    Map<String, Object> getIndex(String name);

    /**
     * 添加数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param content   内容
     * @return
     */
    boolean addData(String indexName, long id, String content);

    /**
     * 添加数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param content   内容
     * @return
     */
    boolean addData(String indexName, long id, Object content);

    /**
     * 添加数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param content   内容
     * @return
     */
    boolean addData(String indexName, long id, Map<String, Object> content);

    /**
     * 返回数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> T getData(String indexName, long id, Class<T> clazz);

    /**
     * 返回数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @return
     */
    Map<String, Object> getData(String indexName, long id);


    /**
     * 存在数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @return
     */
    boolean existsData(String indexName, long id);

    /**
     * 删除数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @return
     */
    boolean deleteData(String indexName, long id);

    /**
     * 更新数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param content   数据
     * @return
     */
    boolean updateData(String indexName, long id, String content);

    /**
     * 更新数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param content   数据
     * @return
     */
    boolean updateData(String indexName, long id, Object content);

    /**
     * 更新数据
     *
     * @param indexName   索引名
     * @param id          索引id
     * @param content     数据
     * @param retryNumber 重试次数
     * @return
     */
    boolean updateData(String indexName, long id, Object content, int retryNumber);

    /**
     * 更新数据
     *
     * @param indexName   索引名
     * @param id          索引id
     * @param content     数据
     * @param retryNumber 重试次数
     * @return
     */
    boolean updateData(String indexName, long id, String content, int retryNumber);

    /**
     * 更新数据
     *
     * @param indexName 索引名
     * @param id        索引id
     * @param content   数据
     * @return
     */
    boolean updateData(String indexName, long id, Map<String, Object> content);


    /**
     * 更新数据
     *
     * @param indexName   索引名
     * @param id          索引id
     * @param content     数据
     * @param retryNumber 重试次数
     * @return
     */
    boolean updateData(String indexName, long id, Map<String, Object> content, int retryNumber);

    /**
     * 搜索，取前10条，超时时间1秒
     *
     * @param indexName 索引名
     * @param key       键名
     * @param value     值
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, String key, String value, Class<T> clazz);

    /**
     * 搜索，取前10条，超时时间1秒
     *
     * @param indexName 索引名
     * @param key       键名
     * @param value     值
     * @return
     */
    Map<String, Object> search(String indexName, String key, String value);

    /**
     * 搜索，取前N条，超时时间1秒
     *
     * @param indexName 索引名
     * @param key       键名
     * @param value     值
     * @param size      返回条数
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, String key, String value, int size, Class<T> clazz);

    /**
     * 搜索，取前N条，超时时间1秒
     *
     * @param indexName 索引名
     * @param key       键名
     * @param value     值
     * @param size      返回条数
     * @return
     */
    Map<String, Object> search(String indexName, String key, String value, int size);

    /**
     * 搜索，超时时间1秒
     *
     * @param indexName 索引名
     * @param key       键名
     * @param value     值
     * @param from      从哪一条开始
     * @param size      取多少条
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, String key, String value, int from, int size, Class<T> clazz);

    /**
     * 搜索，超时时间1秒
     *
     * @param indexName 索引名
     * @param key       键名
     * @param value     值
     * @param from      从哪一条开始
     * @param size      取多少条
     * @return
     */
    Map<String, Object> search(String indexName, String key, String value, int from, int size);

    /**
     * @param indexName    索引名
     * @param key          键名
     * @param value        值
     * @param from         从哪一条开始
     * @param size         取多少条
     * @param milliSeconds 超时时间(豪秒)
     * @param clazz        返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, String key, String value, int from, int size, int milliSeconds, Class<T> clazz);

    /**
     * @param indexName    索引名
     * @param key          键名
     * @param value        值
     * @param from         从哪一条开始
     * @param size         取多少条
     * @param milliSeconds 超时时间(豪秒)
     * @return
     */
    Map<String, Object> search(String indexName, String key, String value, int from, int size, int milliSeconds);

    /**
     * 按条件搜索前10条，超时时间一秒
     *
     * @param indexName 索引名
     * @param search    返回类型
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, BoolSearch search, Class<T> clazz);

    /**
     * 按条件搜索前10条，超时时间一秒
     *
     * @param indexName 索引名
     * @param search    返回类型
     * @return
     */
    Map<String, Object> search(String indexName, BoolSearch search);

    /**
     * 按条件搜索前N条，超时时间一秒
     *
     * @param indexName 索引名
     * @param search    返回类型
     * @param size      取多少条
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, BoolSearch search, int size, Class<T> clazz);

    /**
     * 按条件搜索前N条，超时时间一秒
     *
     * @param indexName 索引名
     * @param search    返回类型
     * @param size      取多少条
     * @return
     */
    Map<String, Object> search(String indexName, BoolSearch search, int size);

    /**
     * 按条件搜索，超时时间一秒
     *
     * @param indexName 索引名
     * @param search    搜索条件
     * @param number    第几页
     * @param size      取多少条
     * @param clazz     返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, BoolSearch search, int number, int size, Class<T> clazz);

    /**
     * 按条件搜索，超时时间一秒
     *
     * @param indexName 索引名
     * @param search    搜索条件
     * @param number    第几页
     * @param size      取多少条
     * @return
     */
    Map<String, Object> search(String indexName, BoolSearch search, int number, int size);

    /**
     * 按条件搜索
     *
     * @param indexName    索引名
     * @param search       搜索条件
     * @param number       第几页
     * @param size         取多少条
     * @param milliSeconds 超时时间(豪秒)
     * @param clazz        返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, BoolSearch search, List<OrderByInput> sorts, int number, int size, int milliSeconds, Class<T> clazz);

    /**
     * 按条件搜索
     *
     * @param indexName    索引名
     * @param search       搜索条件
     * @param sorts        排序
     * @param sortFun      排序函数
     * @param number       第几页
     * @param size         取多少条
     * @param milliSeconds 超时时间(豪秒)
     * @param clazz        返回类型
     * @param <T>
     * @return
     */
    <T> PageContent<T> search(String indexName, BoolSearch search, List<OrderByInput> sorts, SortFun sortFun, int number, int size, int milliSeconds, Class<T> clazz);

    /**
     * 按条件搜索
     *
     * @param indexName    索引名
     * @param search       搜索条件
     * @param number       第几页
     * @param size         取多少条
     * @param milliSeconds 超时时间(豪秒)
     * @return
     */
    Map<String, Object> search(String indexName, BoolSearch search, List<OrderByInput> sorts, int number, int size, int milliSeconds);

    /**
     * 按条件搜索
     *
     * @param indexName    索引名
     * @param search       搜索条件
     * @param sorts        排序
     * @param sortFun      排序条件
     * @param number       第几页
     * @param size         取多少条
     * @param milliSeconds 超时时间(豪秒)
     * @return
     */
    Map<String, Object> search(String indexName, BoolSearch search, List<OrderByInput> sorts, SortFun sortFun, int number, int size, int milliSeconds);
}
