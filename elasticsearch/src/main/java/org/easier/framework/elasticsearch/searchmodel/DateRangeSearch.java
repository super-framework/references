package org.easier.framework.elasticsearch.searchmodel;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DateRangeSearch extends BaseSearch implements Search {

    private String value;
    /**
     * 大于等于
     */
    private LocalDateTime gteValue;
    /**
     * 大于
     */
    private LocalDateTime gtValue;
    /**
     * 小于等于
     */
    private LocalDateTime lteValue;
    /**
     * 小于
     */
    private LocalDateTime ltValue;

    private String timeZone;


}
