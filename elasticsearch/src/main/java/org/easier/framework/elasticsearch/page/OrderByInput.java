package org.easier.framework.elasticsearch.page;

import lombok.Data;

@Data
public class OrderByInput {
    private String propertyName;

    private boolean isDescending;

}
