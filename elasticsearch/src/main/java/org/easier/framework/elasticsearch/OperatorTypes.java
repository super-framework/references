package org.easier.framework.elasticsearch;

public enum OperatorTypes {
    OR,
    AND,
    NOT
}
