package org.easier.framework.elasticsearch.page;

import lombok.Data;

import java.util.List;

@Data
public class PageInput {
    private int pageNumber;
    private int pageSize;
    private List<OrderByInput> orders;

    public int getPageNumber() {
        return pageNumber < 1 ? 1 : Math.min(pageNumber, 1000);
    }

    public int getPageSize() {
        return pageSize < 1 ? 10 : Math.min(pageSize, 100);
    }
}
