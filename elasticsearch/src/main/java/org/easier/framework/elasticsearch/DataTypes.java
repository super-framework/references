package org.easier.framework.elasticsearch;

public enum DataTypes {
    /**
     * 全文搜索
     */
    TEXT("text"),

    /**
     * 全词匹配
     */
    KEYWORD("keyword"),
    INTEGER("integer"),
    LONG("long"),
    SHORT("short"),
    BYTE("byte"),
    BOOLEAN("boolean"),
    DATE("date"),
    /**
     * 范围查询
     */
    RANGE("range"),
    /**
     * 二进制
     */
    BINARY("binary"),
    /**
     * 地理坐标
     */
    GEO_POINT("geo_point"),
    /**
     * 地理地图
     */
    GEO_SHAPE("geo_shape"),
    IP("ip"),
    /**
     * 范围
     */
    COMPLETION("completion"),

    ARRAY("array"),

    OBJECT("object"),

    NESTED("nested")
    ;

    String name;

    DataTypes(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
