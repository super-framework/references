package org.easier.framework.elasticsearch.searchmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.easier.framework.elasticsearch.OperatorTypes;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextSearch extends BaseSearch implements Search {

    private String value;
    private OperatorTypes operatorType;
    private float boost;
}
