package org.easier.framework.elasticsearch.searchmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RangeSearch extends BaseSearch implements Search {


    private String value;
    /**
     * 大于等于
     */
    private Object gteValue;
    /**
     * 大于
     */
    private Object gtValue;
    /**
     * 小于等于
     */
    private Object lteValue;
    /**
     * 小于
     */
    private Object ltValue;
}
