package org.easier.framework.elasticsearch.page;

import lombok.Data;

import java.util.List;

@Data
public class PageContent<T> {

    public PageContent(List<T> items, long total) {
        this.items = items;
        this.total = total;
    }

    private List<T> items;

    private long total;
}
