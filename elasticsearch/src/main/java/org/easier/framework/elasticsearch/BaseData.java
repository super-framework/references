package org.easier.framework.elasticsearch;

public interface BaseData {
    void setId(long id);

    long getId();

    void setScore(float score);

    float getScore();
}
