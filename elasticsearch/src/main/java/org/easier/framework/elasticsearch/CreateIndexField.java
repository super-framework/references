package org.easier.framework.elasticsearch;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateIndexField {

    public CreateIndexField() {
        this.fields = new ArrayList<>();
    }


    public CreateIndexField(String name, DataTypes type, AnalyzerTypes analyzer, boolean index) {
        this(name, type, analyzer, index, false);
    }

    public CreateIndexField(String name, DataTypes type, AnalyzerTypes analyzer, boolean index, boolean isSort) {
        this.name = name;
        this.type = type;
        this.analyzer = analyzer;
        this.index = index;
        this.isSort = isSort;
        this.fields = new ArrayList<>();
    }

    /**
     * 字段类型
     */
    private String name;

    /**
     * 字段的数据类型
     */
    private DataTypes type;

    /**
     * 分词器
     */
    private AnalyzerTypes analyzer;

    /**
     * 是否用于搜索
     */
    private boolean index;

    /**
     * 格式化
     */
    private String format;

    /**
     * 是否用于排序
     */
    private boolean isSort;

    /**
     * 子字段
     */
    private List<CreateIndexField> fields;
}
