package org.easier.framework.redis;


import java.util.Set;

/**
 * CacheUtil 即 CacheAgent
 *
 */
public final class RedisAgent {
    private static RedisAdapter redisAdapter;
    static final String ERROR_MESSAGE = "没有为缓存管理器设置 缓存适配器";

    public static void setCacheAdapter(RedisAdapter redisAdapter) {
        RedisAgent.redisAdapter = redisAdapter;
    }

    public static String get(String key) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.get(key);
    }

    public static <T> T get(String key, Class<T> clazz) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.get(key, clazz);
    }

    public static boolean set(String key, String data) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.set(key, data);
    }

    public static boolean set(String key, Object data) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.set(key, data);
    }

    public static boolean set(String key, String data, int second) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.set(key, data, second);
    }

    public static boolean set(String key, Object data, int second) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.set(key, data, second);
    }

    public static boolean del(String key) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.del(key);
    }

    public static boolean exists(String key) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.exists(key);
    }

    public static boolean setNext(String key, Object data, int second) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.setNext(key, data, second);
    }

    public static boolean mapSet(String mapKey, String key, Object data) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.mapSet(mapKey, key, data);
    }

    public static <T> T mapGet(String mapKey, String key, Class<T> clazz) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.mapGet(mapKey, key, clazz);
    }

    public static boolean mapDel(String mapKey, String key) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.mapDel(mapKey, key);
    }

    public static boolean mapExists(String mapKey, String key) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.mapExists(mapKey, key);
    }

    public static long nextNumber(String key) {
        if (redisAdapter == null)
            throw new RuntimeException(ERROR_MESSAGE);
        return redisAdapter.nextNumber(key);
    }

    public static long nextNumber(String key, int number) {
        if (redisAdapter == null) {
            throw new RuntimeException(ERROR_MESSAGE);
        } else {
            return redisAdapter.nextNumber(key, number);
        }
    }

    public static <T> Set<T> zGet(String key, double minScore, double maxScore, Class<T> clazz) {
        if (redisAdapter == null) {
            throw new RuntimeException(ERROR_MESSAGE);
        } else {
            return redisAdapter.zGet(key, minScore, maxScore, clazz);
        }
    }

    public static boolean zSet(String key, double score, Object object) {
        if (redisAdapter == null) {
            throw new RuntimeException(ERROR_MESSAGE);
        } else {
            return redisAdapter.zSet(key, score, object);
        }
    }

    public static boolean zDel(String key, Object object) {
        if (redisAdapter == null) {
            throw new RuntimeException(ERROR_MESSAGE);
        } else {
            return redisAdapter.zDel(key, object);
        }
    }
}
