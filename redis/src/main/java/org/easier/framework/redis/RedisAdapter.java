package org.easier.framework.redis;

import java.util.Set;

public interface RedisAdapter {
    String get(String key);

    <T> T get(String key, Class<T> clazz);

    boolean set(String key, Object data);

    boolean set(String key, String data);

    boolean set(String key, Object data, int second);

    boolean set(String key, String data, int second);

    boolean del(String key);

    boolean exists(String key);

    boolean setNext(String key, Object data, int second);

    boolean mapSet(String mapKey, String key, Object data);

    <T> T mapGet(String mapKey, String key, Class<T> clazz);

    boolean mapDel(String mapKey, String key);

    boolean mapExists(String mapKey, String key);

    long nextNumber(String key);

    long nextNumber(String key, int number);

    <T> Set<T> zGet(String key, double minScore, double maxScore, Class<T> clazz);

    boolean zSet(String key, double score, Object object);

    boolean zDel(String key, Object object);
}
