package org.easier.framework.beancopier;

import java.util.List;

/**
 * @author 派大星
 */
public interface CopierAdapter {

    /**
     * 把source相同字段名属性拷入target
     *
     * @param source
     * @param target
     * @param <T>
     * @return
     */
    <T> T map(Object source, T target);

    /**
     * 创建targetClass对象 并把source相同字段名属性拷入
     * 根据字段名拷贝值
     *
     * @param source      源对象
     * @param targetClass 目标类
     * @param <T>
     * @return
     */
    <T> T map(Object source, Class<T> targetClass);


    /**
     * 创建targetClass对象 并把source相同字段名属性拷入
     * 根据字段名拷贝值
     *
     * @param source      源对象
     * @param targetClass 目标类
     * @param <T>
     * @return
     */
    <T> List<T> mapList(Object sources, Class<T> targetClass);
}
