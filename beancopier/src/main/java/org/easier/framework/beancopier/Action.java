package org.easier.framework.beancopier;


@FunctionalInterface
public interface Action<T> {
    void run(T t);
}
