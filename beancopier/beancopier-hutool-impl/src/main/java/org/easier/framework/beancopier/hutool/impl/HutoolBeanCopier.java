package org.easier.framework.beancopier.hutool.impl;

import cn.hutool.core.bean.BeanUtil;
import org.easier.framework.beancopier.CopierAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * cn.hutool.core.bean.BeanUtil
 */
public class HutoolBeanCopier implements CopierAdapter {
    @Override
    public <T> T map(Object source, T target) {
        BeanUtil.copyProperties(source, target);
        return target;
    }

    @Override
    public <T> T map(Object source, Class<T> targetClass) {
        return BeanUtil.toBeanIgnoreError(source, targetClass);
    }

    @Override
    public <T> List<T> mapList(Object sources, Class<T> targetClass) {
        List<T> list = new ArrayList<>();
        if (sources instanceof List) {
            for (Object source : (List) sources) {
                list.add(map(source, targetClass));
            }
        }
        return list;
    }


}
