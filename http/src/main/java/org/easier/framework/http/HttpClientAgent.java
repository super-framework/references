package org.easier.framework.http;


/**
 * HttpClientAgent
 * 非适配器
 * 非代理模式
 */
public class HttpClientAgent {
    private static HttpAdapter httpAdapter;

    public static void init(HttpAdapter httpAdapter) {
        HttpClientAgent.httpAdapter = httpAdapter;
    }

    public static HttpAdapter getHttpAdapter() {
        if (httpAdapter == null) {
            throw new RuntimeException("没有为HttpUtil初始化");
        }
        return httpAdapter;
    }
}
