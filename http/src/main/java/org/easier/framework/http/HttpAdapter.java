package org.easier.framework.http;

import java.util.Map;

public interface HttpAdapter {

    /**
     * httpGet请求
     *
     * @param uri       uri地址
     * @param paramMap  请求参数
     * @param headerMap http请求头
     * @return http返回值
     */
    HttpResult get(String uri, Map<String, String> paramMap, Map<String, String> headerMap);

    /**
     * postFrom请求
     *
     * @param uri       uri地址
     * @param paramMap  请求参数
     * @param headerMap http请求头
     * @return http返回值
     */
    HttpResult postForm(String uri, Map<String, String> paramMap, Map<String, String> headerMap);

    /**
     * postJson请求
     *
     * @param uri         uri地址
     * @param requestJson 请求参数
     * @param headerMap   http请求头
     * @return http返回值
     */
    HttpResult postJson(String uri, String requestJson, Map<String, String> headerMap);

    /**
     * postJson请求
     *
     * @param requestJson 请求参数
     * @return http返回值
     */
    <T> T postJson(String uri, Object requestJson, Map<String, String> headerMap, Class<T> resultClass);
}
