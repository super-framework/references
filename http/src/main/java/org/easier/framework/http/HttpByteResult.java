package org.easier.framework.http;

public class HttpByteResult {
    public HttpByteResult(int httpCode, byte[] content) {
        this.httpCode = httpCode;
        this.content = content;
    }

    private int httpCode;
    private byte[] content;

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
