package org.easier.framework.http;

public class HttpResult {

    public HttpResult(int httpCode, String content) {
        this.httpCode = httpCode;
        this.content = content;
    }

    private int httpCode;
    private String content;

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
