package org.easier.framework.json;

import java.util.List;
import java.util.Map;

public interface JsonAdapter {
    String serialize(Object entity);

    String serialize(Object entity, boolean notNull);

    <T> T deserialize(String jsonContent, Class<T> clazz);

    <T> T toObject(String content,Class<T> clazz);

    <T> T toObject(String content,String key,Class<T> clazz);

    <T> List<T> deserializeList(String jsonContent, Class<T> clazz);

    <K, V> Map<K, V> deserializeMap(String jsonContent, Class<K> keyClazz, Class<V> valueClazz);
}
