package org.easier.framework.json;


import java.util.List;
import java.util.Map;

/**
 * json序列化工具类
 */
public class JsonAgent {
    private static JsonAdapter adapter;

    public static void init(JsonAdapter jsonAdapter) {
        JsonAgent.adapter = jsonAdapter;
    }

    public static String serialize(Object entity) {
        if (adapter == null)
            throw new RuntimeException("没有为json序列化指定设配器");
        return adapter.serialize(entity);
    }

    public static String serialize(Object entity, boolean notNull) {
        if (adapter == null)
            throw new RuntimeException("没有为json序列化指定设配器");
        return adapter.serialize(entity, notNull);
    }

    public static <T> T deserialize(String jsonContent, Class<T> clazz) {
        if (adapter == null)
            throw new RuntimeException("没有为json序列化指定设配器");
        return adapter.deserialize(jsonContent, clazz);
    }

    public static <T> T toObject(String content, String key, Class<T> clazz) {
        if (adapter == null)
            throw new RuntimeException("没有为json序列化指定设配器");
        return adapter.toObject(content, key, clazz);
    }

    public static <T> List<T> deserializeList(String jsonContent, Class<T> clazz) {
        if (adapter == null)
            throw new RuntimeException("没有为json序列化指定设配器");
        return adapter.deserializeList(jsonContent, clazz);
    }

    public static <K, V> Map<K, V> deserializeMap(String jsonContent, Class<K> keyClazz, Class<V> valueClazz) {
        if (adapter == null)
            throw new RuntimeException("没有为json序列化指定设配器");
        return adapter.deserializeMap(jsonContent, keyClazz, valueClazz);
    }

}
