package org.easier.framework.fastjson.impl;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateToStringSerializer implements ObjectSerializer {
    public static final DateToStringSerializer instance = new DateToStringSerializer();

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        SerializeWriter out = serializer.out;

        if (object == null) {
            out.writeNull();
            return;
        }

        long strVal = ((LocalDateTime) object).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        out.writeLong(strVal);
    }
}
