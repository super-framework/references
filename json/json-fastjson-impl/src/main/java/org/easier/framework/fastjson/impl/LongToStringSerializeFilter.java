package org.easier.framework.fastjson.impl;

import com.alibaba.fastjson.serializer.ValueFilter;

public class LongToStringSerializeFilter implements ValueFilter {
    @Override
    public Object process(Object object, String name, Object value) {
        if (value == null) {
            return value;
        }
        Class clazz = value.getClass();
        if (clazz == Long.class)
            return value.toString();
        return value;
    }
}
